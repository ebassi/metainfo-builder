MetaInfo Builder
================

[![pipeline status](https://gitlab.gnome.org/ebassi/metainfo-builder/badges/main/pipeline.svg)](https://gitlab.gnome.org/ebassi/metainfo-builder/-/commits/main)

A small application to edit and create [AppStream metadata files][appstream].

MetaInfo Builder allows you to open existing MetaInfo files, and write your own
ones.

Code of conduct
---------------

Amberol follows the GNOME project [Code of Conduct](./code-of-conduct.md). All
communications in project spaces, such as the issue tracker or
[Discourse](https://discourse.gnome.org) are expected to follow it.

Copyright and licensing
-----------------------

Copyright 2022  Emmanuele Bassi

MetaInfo Builder is released under the terms of the Apache License, version 2.0.

[appstream]: https://www.freedesktop.org/software/appstream/docs/chap-Metadata.htmll
