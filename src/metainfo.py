# SPDX-FileCopyrightText: 2022  Emmanuele Bassi
# SPDX-License-Identifier: Apache-2.0

import copy
import xml.etree.ElementTree as etree

from gi.repository import GObject

from typing import Callable, Dict, List, Mapping, Optional


def description_to_string(node: etree.Element) -> str:
    res = []
    for child in node:
        tree = etree.ElementTree(copy.deepcopy(child))
        etree.indent(tree)
        res += [
            etree.tostring(tree.getroot(), encoding='unicode', method='xml'),
        ]
    return "\n".join(res)


class MetadataElement:
    '''A serializable metadata element inside the AppStream specification.

    https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#spec-component-filespec
    '''

    def toxml(self) -> etree.Element:
        return etree.Element('invalid')

    @classmethod
    def fromxml(cls, node: etree.Element) -> object:
        pass


class Screenshot(MetadataElement):

    def __init__(self, image: str, caption: str = None):
        self.image = image
        self.caption = caption

    def toxml(self) -> etree.Element:
        screenshot = etree.Element('screenshot')
        image = etree.SubElement(screenshot, 'image')
        image.text = self.image
        if self.caption:
            caption = etree.SubElement(screenshot, 'caption')
            caption.text = self.caption
        return screenshot

    @classmethod
    def fromxml(cls, node: etree.Element) -> 'Screenshot':
        assert node.tag == 'screenshot'
        image = None
        caption = None
        for child in node:
            if child.tag == 'image':
                image = child.text
            elif child.tag == 'caption':
                caption = child.text
        assert image is not None
        return cls(image=image, caption=caption)


class Requirement(MetadataElement):
    pass


class Compare:
    EQ = 'eq'
    NE = 'ne'
    LT = 'lt'
    GT = 'gt'
    LE = 'le'
    GE = 'ge'


class Control(Requirement):

    def __init__(self, value: str):
        self.value = value

    def toxml(self) -> etree.Element:
        control = etree.Element('control')
        control.text = self.value
        return control

    @classmethod
    def fromxml(cls, node: etree.Element) -> 'Control':
        assert node.tag == 'control'
        assert node.text is not None
        return cls(value=node.text)


class DisplayLength(Requirement):

    def __init__(self, value: str, compare: str = Compare.GE):
        self.compare = compare
        self.value = value

    def toxml(self) -> etree.Element:
        display_length = etree.Element('display_length')
        display_length.set('compare', self.compare)
        display_length.text = self.value
        return display_length

    @classmethod
    def fromxml(cls, node: etree.Element) -> 'DisplayLength':
        assert node.tag == 'display_length'
        assert node.text is not None
        compare = node.attrib.get('compare', Compare.GE)
        return cls(compare=compare, value=node.text)


class ReleaseUrgency:
    LOW = 'low'
    MEDIUM = 'medium'
    HIGH = 'high'
    CRITICAL = 'critical'


class ReleaseType:
    STABLE = 'stable'
    DEVELOPMENT = 'development'


class Release(MetadataElement):

    def __init__(self, version: str,
                 release_type: str = ReleaseType.STABLE,
                 date: Optional[str] = None,
                 urgency: str = ReleaseUrgency.MEDIUM,
                 description: Optional[str] = None,
                 url: Optional[str] = None):
        self.version = version
        self.date = date
        self.description = description
        self.release_type = release_type
        self.urgency = urgency
        self.url = url

    def toxml(self) -> etree.Element:
        release = etree.Element('release')
        release.set('version', self.version)
        if self.date:
            release.set('date', self.date)
        release.set('urgency', self.urgency)
        release.set('type', self.release_type)
        if self.description:
            description = etree.fromstring("\n".join([
                '<description>',
                self.description,
                '</description>',
            ]))
            release.append(description)
        if self.url:
            url = etree.SubElement(release, 'url')
            url.text = self.url
        return release

    @classmethod
    def fromxml(cls, node: etree.Element) -> 'Release':
        assert node.tag == 'release'
        assert 'version' in node.attrib
        version = node.attrib['version']
        date = node.attrib.get('date')
        urgency = node.attrib.get('urgency', ReleaseUrgency.MEDIUM)
        release_type = node.attrib.get('type', ReleaseType.STABLE)
        url = None
        description = None
        for child in node:
            if child.tag == 'url':
                url = child.text
            elif child.tag == 'description':
                description = description_to_string(child)
        return cls(version=version, date=date, urgency=urgency,
                   release_type=release_type, url=url,
                   description=description)


class RatingType:
    '''Type for <content_rating/> element

    https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-content_rating
    '''
    OARS10 = 'oars-1.0'
    OARS11 = 'oars-1.1'


class RatingValue:
    '''Value for the <content_attribute/> element

    https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-content_rating
    '''
    NONE = 'none'
    MILD = 'mild'
    MODERATE = 'moderate'
    INTENSE = 'intense'


class ContentRating(MetadataElement):
    '''Content Rating element

    https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-content_rating
    '''

    def __init__(self, rating_type: str = RatingType.OARS11,
                 attrib: Dict[str, str] = {}) -> None:
        self.rating_type = rating_type
        self.attributes = attrib

    def add_attribute(self, attribute_id: str, value: str) -> None:
        '''Add a new content rating attribute.'''
        self.attributes[attribute_id] = value

    def toxml(self) -> etree.Element:
        content_rating = etree.Element('content_rating')
        content_rating.set('type', self.rating_type)
        for attr in self.attributes.keys():
            attribute = etree.SubElement(content_rating, 'content_attribute')
            attribute.set('id', attr)
            attribute.text = self.attributes[attr]
        return content_rating

    @classmethod
    def fromxml(cls, node: etree.Element) -> 'ContentRating':
        assert node.tag == 'content_rating'
        rating_type = node.attrib.get('type', RatingType.OARS11)
        attrs = {}
        for child in node:
            if child.tag == 'content_attribute' and 'id' in child.attrib:
                attribute = child.attrib['id']
                attrs[attribute] = child.text or ''
        return cls(rating_type, attrs)


class TranslationType:
    GETTEXT = 'gettext'
    QT = 'qt'


class Translation(MetadataElement):

    def __init__(self, translation_type: str, value: str):
        self.translation_type = translation_type
        self.value = value

    def toxml(self) -> etree.Element:
        translation = etree.Element('translation')
        translation.set('type', self.translation_type)
        translation.text = self.value
        return translation

    @classmethod
    def fromxml(cls, node: etree.Element) -> 'Translation':
        assert node.tag == 'translation'
        assert node.text is not None
        translation_type = node.attrib.get('type', TranslationType.GETTEXT)
        return cls(translation_type=translation_type, value=node.text)


class ComponentType:
    pass


class MetaInfo(GObject.Object):

    __gtype_name__ = 'MetaInfo'

    component_type = GObject.Property(
        type=str,
        flags=GObject.ParamFlags.READWRITE,
        default='desktop-application')

    def __init__(self, component: str = 'desktop-application'):
        super().__init__(component_type=component)

        self._id: Optional[str] = None
        self._name: Optional[str] = None
        self._summary: Optional[str] = None
        self._description: Optional[str] = None
        self._launchables: Dict[str, str] = {}
        self._developer_name: Optional[str] = None
        self._update_contact: Optional[str] = None
        self._project_group: Optional[str] = None
        self._project_license: str = "CC0-1.0"
        self._metadata_license: str = "CC0-1.0"
        self._custom: Dict[str, str] = {}
        self._screenshots: List[Screenshot] = []
        self._kudos: List[str] = []
        self._supports: List[Requirement] = []
        self._requires: List[Requirement] = []
        self._recommends: List[Requirement] = []
        self._releases: List[Release] = []
        self._content_rating: Optional[ContentRating] = None
        self._provides: Optional[str] = None
        self._translations: List[Translation] = []

    @GObject.Property(type=str)
    def id(self) -> Optional[str]:
        return self._id

    @id.setter
    def set_id(self, app_id: Optional[str]) -> None:
        if self._id != app_id:
            self._id = app_id

    @GObject.Property(type=str,
                      flags=GObject.ParamFlags.READWRITE | GObject.ParamFlags.EXPLICIT_NOTIFY)
    def name(self) -> Optional[str]:
        return self._name

    @name.setter
    def set_name(self, name: Optional[str]) -> None:
        if self._name != name:
            self._name = name
            self.notify('name')

    @GObject.Property(type=str)
    def summary(self) -> Optional[str]:
        return self._summary

    @summary.setter
    def set_summary(self, summary: Optional[str]) -> None:
        if self._summary != summary:
            self._summary = summary

    @GObject.Property(type=str)
    def description(self) -> Optional[str]:
        return self._description

    @description.setter
    def set_description(self, description: Optional[str]) -> None:
        if self._description != description:
            self._description = description

    def add_launchable(self, launchable: str,
                       type_: str = "desktop-id") -> None:
        self._launchables[type_] = launchable

    @GObject.Property(type=str)
    def project_license(self) -> Optional[str]:
        return self._project_license

    @project_license.setter
    def set_project_license(self, spdx: Optional[str]) -> None:
        if self._project_license != spdx:
            self._project_license = spdx or 'CC0-1.0'

    @GObject.Property(type=str)
    def metadata_license(self) -> Optional[str]:
        return self._metadata_license

    @metadata_license.setter
    def set_metadata_license(self, spdx: Optional[str]) -> None:
        if self._metadata_license != spdx:
            self._metadata_license = spdx or 'CC0-1.0'

    @GObject.Property(type=str)
    def developer_name(self) -> Optional[str]:
        return self._developer_name

    @developer_name.setter
    def set_developer_name(self, name: Optional[str]) -> None:
        if self._developer_name != name:
            self._developer_name = name

    @GObject.Property(type=str)
    def update_contact(self) -> Optional[str]:
        return self._update_contact

    @update_contact.setter
    def set_update_contact(self, contact: Optional[str]) -> None:
        if self._update_contact != contact:
            self._update_contact = contact

    @GObject.Property(type=str)
    def project_group(self) -> Optional[str]:
        return self._project_group

    @project_group.setter
    def set_project_group(self, group: Optional[str]) -> None:
        if self._project_group != group:
            self._project_group = group

    def validate(self) -> None:
        if self._id is None:
            raise ValueError("Invalid application id")

    def _parse_id(self, node: etree.Element) -> None:
        self.props.id = node.text

    def _parse_metadata_license(self, node: etree.Element) -> None:
        self.props.metadata_license = node.text

    def _parse_project_license(self, node: etree.Element) -> None:
        self.props.project_license = node.text

    def _parse_name(self, node: etree.Element) -> None:
        self.props.name = node.text

    def _parse_summary(self, node: etree.Element) -> None:
        self.props.summary = node.text

    def _parse_description(self, node: etree.Element) -> None:
        self.props.description = description_to_string(node)

    def _parse_screenshots(self, node: etree.Element) -> None:
        assert node.tag == 'screenshots'
        for child in node:
            if child.tag == 'screenshot':
                self._screenshots.append(Screenshot.fromxml(child))

    def _parse_launchable(self, node: etree.Element) -> None:
        pass

    def _parse_url(self, node: etree.Element) -> None:
        pass

    def _parse_developer_name(self, node: etree.Element) -> None:
        self.props.developer_name = node.text

    def _parse_update_contact(self, node: etree.Element) -> None:
        self.props.update_contact = node.text

    def _parse_project_group(self, node: etree.Element) -> None:
        self.props.project_group = node.text

    def _parse_translation(self, node: etree.Element) -> None:
        self._translations.append(Translation.fromxml(node))

    def _parse_custom(self, node: etree.Element) -> None:
        pass

    def _parse_requires(self, node: etree.Element) -> None:
        pass

    def _parse_recommends(self, node: etree.Element) -> None:
        pass

    def _parse_releases(self, node: etree.Element) -> None:
        pass

    def _parse_kudos(self, node: etree.Element) -> None:
        pass

    def _parse_content_rating(self, node: etree.Element) -> None:
        self._content_rating = ContentRating.fromxml(node)

    def _parse_provides(self, node: etree.Element) -> None:
        pass

    def _parse_tree(self, root: etree.Element) -> None:
        assert root.tag == 'component'

        self.component_type = root.attrib.get('component-type',
                                              'desktop-application')

        elements: Mapping[str, Callable[[etree.Element], None]] = {
            'id': self._parse_id,
            'metadata_license': self._parse_metadata_license,
            'project_license': self._parse_project_license,
            'name': self._parse_name,
            'summary': self._parse_summary,
            'description': self._parse_description,
            'screenshots': self._parse_screenshots,
            'launchable': self._parse_launchable,
            'url': self._parse_url,
            'developer_name': self._parse_developer_name,
            'update_contact': self._parse_update_contact,
            'project_group': self._parse_project_group,
            'translation': self._parse_translation,
            'custom': self._parse_custom,
            'requires': self._parse_requires,
            'recommends': self._parse_recommends,
            'releases': self._parse_releases,
            'kudos': self._parse_kudos,
            'content_rating': self._parse_content_rating,
            'provides': self._parse_provides,
        }

        for node in root:
            parser_method = elements.get(node.tag)
            if parser_method:
                parser_method(node)

    @classmethod
    def fromstring(cls, xml: str) -> 'MetaInfo':
        root = etree.fromstring(xml)
        component_type = root.get('type')
        if not component_type:
            component_type = 'desktop-application'
        metainfo = cls(component=component_type)
        metainfo._parse_tree(root)
        return metainfo

    def parse(self, xml: str) -> None:
        self._parse_tree(etree.fromstring(xml))

    def dump(self) -> str:
        component = etree.Element('component',
                                  attrib={'type': self.props.component_type})

        id = etree.SubElement(component, "id")
        id.text = self._id

        if self._name:
            name = etree.SubElement(component, "name")
            name.text = self._name

        if self._summary:
            summary = etree.SubElement(component, "summary")
            summary.text = self._summary

        if self._description:
            xml = etree.fromstring("\n".join([
                "<description>",
                self._description,
                "</description>",
            ]))
            component.append(xml)

        for type_ in self._launchables.keys():
            launchable = etree.SubElement(component,
                                          "launchable",
                                          attrib={"type": type_})
            launchable.text = self._launchables[type_]

        project_license = etree.SubElement(component, "project_license")
        project_license.text = self._project_license

        metadata_license = etree.SubElement(component, "metadata_license")
        metadata_license.text = self._metadata_license

        if self._developer_name:
            developer_name = etree.SubElement(component, "developer_name")
            developer_name.text = self._developer_name

        if self._update_contact:
            update_contact = etree.SubElement(component, "update_contact")
            update_contact.text = self._update_contact

        if self._project_group:
            project_group = etree.SubElement(component, "project_group")
            project_group.text = self._project_group

        if len(self._recommends) != 0:
            recommends = etree.SubElement(component, "recommends")
            for rec in self._recommends:
                recommends.append(rec.toxml())

        if len(self._kudos) != 0:
            kudos = etree.SubElement(component, "kudos")
            for k in self._kudos:
                kudo = etree.SubElement(kudos, "kudo")
                kudo.text = k

        if self._content_rating:
            component.append(self._content_rating.toxml())
        else:
            component.append(ContentRating().toxml())

        provides = etree.SubElement(component, "provides")
        provides.text = f"{self._id}.desktop"

        tree = etree.ElementTree(component)
        etree.indent(tree)

        xmlheader = '<?xml version="1.0" encoding="UTF-8"?>'
        xmltree = etree.tostring(tree.getroot(),
                                 method='xml',
                                 encoding='unicode')
        xmlbody = '\n'.join([xmlheader, xmltree])

        return xmlbody
