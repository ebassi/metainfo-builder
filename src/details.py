# SPDX-FileCopyrightText: 2022  Emmanuele Bassi
# SPDX-License-Identifier: Apache-2.0

from typing import Optional

from gi.repository import GObject, Gtk


@Gtk.Template(resource_path='/io/bassi/MetaInfoBuilder/ui/details.ui')
class MetaInfoDetailsBox(Gtk.Box):
    __gtype_name__ = 'MetaInfoDetailsBox'

    details_id_row = Gtk.Template.Child()
    details_name_row = Gtk.Template.Child()
    details_summary_row = Gtk.Template.Child()
    details_description_textview = Gtk.Template.Child()

    _application_name: Optional[str] = None
    _application_id: Optional[str] = None
    _summary: Optional[str] = None
    _description: Optional[str] = None

    def __init__(self) -> None:
        super().__init__()

        self.details_id_row.connect("notify::text",
                                    self._update_app_id)
        self.details_name_row.connect("notify::text",
                                      self._update_app_name)
        self.details_summary_row.connect("notify::text",
                                         self._update_app_summary)
        buffer = self.details_description_textview.get_buffer()
        buffer.connect("notify::text", self._update_app_description)

    @GObject.Property(type=str)
    def application_id(self) -> Optional[str]:
        return self._application_id

    @application_id.setter
    def set_application_id(self, app_id: str) -> None:
        if self._application_id != app_id:
            self._application_id = app_id
            self.details_id_row.set_text(app_id)

    @GObject.Property(type=str)
    def application_name(self) -> Optional[str]:
        return self._application_name

    @application_name.setter
    def set_application_name(self, app_name: str) -> None:
        if self._application_name != app_name:
            self._application_name = app_name
            self.details_name_row.set_text(app_name)
            self.notify('application-name')

    @GObject.Property(type=str)
    def summary(self) -> Optional[str]:
        return self._summary

    @summary.setter
    def set_summary(self, summary: str) -> None:
        if self._summary != summary:
            self._summary = summary
            self.details_summary_row.set_text(summary)

    @GObject.Property(type=str)
    def description(self) -> Optional[str]:
        return self._description

    @description.setter
    def set_description(self, description: str) -> None:
        if self._description != description:
            self._description = description
            buffer = self.details_description_textview.get_buffer()
            buffer.set_text(description)

    def _update_app_id(self, row, pspec):
        app_id = row.get_text()
        if app_id:
            row.remove_css_class("error")
            self._application_id = app_id
            self.notify('application-id')
        else:
            row.add_css_class("error")

    def _update_app_name(self, row, pspec):
        self._application_name = row.get_text()
        self.notify('application-name')

    def _update_app_summary(self, row, pspec):
        self._summary = row.get_text()
        self.notify('summary')

    def _update_app_description(self, buffer, pspec):
        start = buffer.get_start_iter()
        end = buffer.get_end_iter()
        self._description = buffer.get_text(start, end, False)
        self.notify('description')
