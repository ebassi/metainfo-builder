# main.py
#
# SPDX-FileCopyrightText: 2022 Emmanuele Bassi
# SPDX-License-Identifier: Apache-2.0

import sys
import gi

gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')

from gi.repository import Gtk, Gio, Adw
from .window import MetaInfoBuilderWindow


GITLAB_URL = 'https://gitlab.gnome.org/ebassi/metainfo-builder'


class MetaInfoBuilderApplication(Adw.Application):
    """The main application singleton class."""

    def __init__(self, app_id, version, profile):
        super().__init__(application_id=app_id,
                         flags=Gio.ApplicationFlags.FLAGS_NONE,
                         resource_base_path='/io/bassi/MetaInfoBuilder')

        self.version = version
        self.profile = profile

        self.create_action('quit', self.quit, ['<primary>q'])
        self.create_action('open', self.open, ['<primary>o'])
        self.create_action('save', self.save, ['<primary>s'])
        self.create_action('about', self.on_about_action)

    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = MetaInfoBuilderWindow(application=self)
            if self.profile == 'development':
                win.add_css_class('devel')
        win.present()

    def on_about_action(self, widget, _):
        about = Adw.AboutWindow(
            transient_for=self.props.active_window,
            application_name='MetaInfo Builder',
            application_icon='io.bassi.MetaInfoBuilder',
            version=self.version,
            copyright='2022 Emmanuele Bassi',
            license_type=Gtk.License.APACHE_2_0,
            developers=['Emmanuele Bassi'],
            issue_url=f'{GITLAB_URL}/-/issues',
            website=GITLAB_URL,
        )
        about.present()

    def open(self, action, _):
        win = self.props.active_window
        self._dialog = Gtk.FileChooserNative(
            title="Open MetaInfo",
            transient_for=win,
            action=Gtk.FileChooserAction.OPEN,
            accept_label="_Open",
            cancel_label="_Cancel",
        )
        self._dialog.connect("response", self.on_open_response)
        self._dialog.show()

    def on_open_response(self, dialog, response):
        if response == Gtk.ResponseType.ACCEPT:
            self.props.active_window.open_metadata(dialog.get_file())
        self._dialog = None

    def save(self, action, _):
        win = self.props.active_window
        self._dialog = Gtk.FileChooserNative(
            title="Save MetaInfo",
            transient_for=win,
            action=Gtk.FileChooserAction.SAVE,
            accept_label="_Save",
            cancel_label="_Cancel",
        )
        self._dialog.connect("response", self.on_save_response)
        self._dialog.show()

    def on_save_response(self, dialog, response):
        if response == Gtk.ResponseType.ACCEPT:
            self.props.active_window.save_metadata(dialog.get_file())
        self._dialog = None

    def create_action(self, name, callback, shortcuts=None):
        action = Gio.SimpleAction.new(name, None)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)


def main(app_id, version, profile):
    """The application's entry point."""
    app = MetaInfoBuilderApplication(app_id, version, profile)
    return app.run(sys.argv)
