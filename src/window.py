# window.py
#
# SPDX-FileCopyrightText: 2022 Emmanuele Bassi
# SPDX-License-Identifier: Apache-2.0

from gi.repository import Adw, GLib, GObject, Gio, Gtk

from .author import MetaInfoAuthorBox  # noqa
from .details import MetaInfoDetailsBox  # noqa
from .licensing import MetaInfoLicensingBox, LicenseType  # noqa
from .metainfo import MetaInfo


@Gtk.Template(resource_path='/io/bassi/MetaInfoBuilder/ui/window.ui')
class MetaInfoBuilderWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'MetaInfoBuilderWindow'

    leaflet = Gtk.Template.Child()
    page_stack = Gtk.Template.Child()
    toast_overlay = Gtk.Template.Child()
    details = Gtk.Template.Child()
    licensing = Gtk.Template.Child()
    license_row = Gtk.Template.Child()
    author = Gtk.Template.Child()
    author_row = Gtk.Template.Child()

    def __new__(cls, *args, **kwargs):
        # XXX: We need to install the actions on the class before
        # the template is initialized, otherwise the widgets inside
        # the templates won't be able to find the actions
        cls._setup_actions()
        return super().__new__(cls, args, kwargs)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self._metadata = MetaInfo()
        self._setup_bindings()

    @classmethod
    def _setup_actions(cls):
        cls.install_action("metainfo.back", None, cls._on_back_activate)
        cls.install_action("metainfo.subpage", "s", cls._on_subpage_activate)

    def _on_back_activate(widget, action, params):
        widget.leaflet.navigate(Adw.NavigationDirection.BACK)

    def _on_subpage_activate(widget, action, params):
        print(f"action: {action}, params: {params}")
        widget.page_stack.set_visible_child_name(params.unpack())
        widget.leaflet.navigate(Adw.NavigationDirection.FORWARD)

    def _add_toast(self, msg):
        toast = Adw.Toast(title=msg)
        self.toast_overlay.add_toast(toast)

    def _on_license_notify(self, _obj, _pspec):
        project = self._metadata.project_license
        metadata = self._metadata.metadata_license
        subtitle = f"Project license: {project}, metadata license: {metadata}"
        self.license_row.props.subtitle = subtitle

    def _on_author_notify(self, _obj, _pspec):
        dev_name = self._metadata.developer_name
        if dev_name:
            subtitle = f"Developer: {dev_name}"
            self.author_row.props.subtitle = subtitle
        else:
            self.author_row.props.subtitle = ""

    def save_metadata(self, output_file):
        self._metadata.validate()
        bytes = GLib.Bytes.new(self._metadata.dump().encode('utf-8'))
        output_file.replace_contents_bytes_async(bytes,
                                                 None,
                                                 False,
                                                 Gio.FileCreateFlags.NONE,
                                                 None,
                                                 self._save_metadata_done)

    def _save_metadata_done(self, file, result):
        res = file.replace_contents_finish(result)
        info = file.query_info("standard::display-name",
                               Gio.FileQueryInfoFlags.NONE)
        if info:
            display_name = info.get_attribute_string("standard::display-name")
        else:
            display_name = file.get_basename()
        if not res:
            msg = f"Unable to save MetaInfo file “{display_name}”"
            self._add_toast(msg)
        else:
            msg = f"Saved MetaInfo file “{display_name}”"
            self._add_toast(msg)

    def open_metadata(self, input_file):
        input_file.load_contents_async(None, self._open_metadata_done)

    def _open_metadata_done(self, file, result):
        contents = file.load_contents_finish(result)
        info = file.query_info("standard::display-name",
                               Gio.FileQueryInfoFlags.NONE)
        if info:
            display_name = info.get_attribute_string("standard::display-name")
        else:
            display_name = file.get_basename()
        if not contents[0]:
            path = file.peek_path()
            err = contents[1]
            print(f"Error while opening {path}: {err}")
            msg = f"Unable to open MetaInfo file “{display_name}”"
            self._add_toast(msg)
        else:
            self._metadata = MetaInfo.fromstring(contents[1])
            self._setup_bindings()

    def _setup_bindings(self):
        flags = GObject.BindingFlags.BIDIRECTIONAL | \
            GObject.BindingFlags.SYNC_CREATE

        self._metadata.bind_property("id",
                                     self.details,
                                     "application-id",
                                     flags)
        self._metadata.bind_property("name",
                                     self.details, "application-name",
                                     flags)
        self._metadata.bind_property("summary",
                                     self.details, "summary",
                                     flags)
        self._metadata.bind_property("description",
                                     self.details, "description",
                                     flags)

        self._metadata.bind_property("project-license",
                                     self.licensing, "project-license",
                                     flags)
        self._metadata.bind_property("metadata-license",
                                     self.licensing, "metadata-license",
                                     flags)
        self._metadata.connect("notify::project-license",
                               self._on_license_notify)
        self._metadata.connect("notify::metadata-license",
                               self._on_license_notify)
        self._on_license_notify(None, None)

        self._metadata.bind_property("developer-name",
                                     self.author, "developer-name",
                                     flags)
        self._metadata.bind_property("project-group",
                                     self.author, "project-group",
                                     flags)
        self._metadata.bind_property("update-contact",
                                     self.author, "update-contact",
                                     flags)
        self._metadata.connect("notify::developer-name",
                               self._on_author_notify)
        self._on_author_notify(None, None)
