# SPDX-FileCopyrightText: 2022  Emmanuele Bassi
# SPDX-License-Identifier: Apache-2.0

from gi.repository import Adw, Gio, GObject, Gtk


PROJECT_LICENSES = {
    'AGPL-3.0-or-later': 'GNU Affero General Public License, version 3.0 or later',  # noqa
    'Apache-2.0': 'Apache License, version 2.0',
    'BSD-3': '3-clause BSD license',
    'BSD': 'BSD standard license',
    'GPL-2.0-or-later': 'GNU General Public License, version 2.0 or later',
    'GPL-3.0-or-later': 'GNU General Public License, version 3.0 or later',
    'LGPL-2.1-or-later': 'GNU Lesser General Public License, version 2.1 or later',  # noqa
    'LGPL-3.0-or-later': 'GNU Lesser General Public License, version 3.0 or later',  # noqa
    'MIT': 'MIT/X11 standard license',
    'MPL-2.0': 'Mozilla Public License, version 2.0',
}

METADATA_LICENSES = {
    'BSL-1.0': 'Boost Software License, version 1.0',
    'CC0-1.0': 'Creative Commons 0, version 1.0',
    'CC-BY-3.0': 'Creative Commons BY, version 3.0',
    'CC-BY-4.0': 'Creative Commons BY, version 4.0',
    'CC-BY-SA-3.0': 'Creative Commons BY-SA, version 3.0',
    'CC-BY-SA-4.0': 'Creative Commons BY-SA, version 4.0',
    'FSFAP': 'FSF All Permissive License',
    'FSFUL': 'FSF Unlimited License',
    'FTL': 'Freetype Project License',
    'GFDL-1.1': 'GNU Free Documentation License, version 1.1',
    'GFDL-1.2': 'GNU Free Documentation License, version 1.2',
    'GFDL-1.3': 'GNU Free Documentation License, version 1.3',
    'MIT': 'MIT/X11 standard license',
    '0BSD': '0-clause BSD license',
}


class LicenseType(GObject.Object):
    __gtype_name__ = 'LicenseType'

    def __init__(self, license_id, license_name):
        super().__init__()

        self._license_id = license_id
        self._license_name = license_name

    @GObject.Property(type=str)
    def license_id(self):
        return self._license_id

    @GObject.Property(type=str)
    def license_name(self):
        return self._license_name

    def __str__(self):
        return f"{self._license_name} ({self._license_id})"


@Gtk.Template(resource_path='/io/bassi/MetaInfoBuilder/ui/licensing.ui')
class MetaInfoLicensingBox(Adw.Bin):
    __gtype_name__ = 'MetaInfoLicensingBox'

    licensing_project_row = Gtk.Template.Child()
    licensing_metadata_row = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self._project_licenses = Gio.ListStore(item_type=LicenseType)
        for spdx in PROJECT_LICENSES:
            self._project_licenses.append(
                LicenseType(spdx, PROJECT_LICENSES[spdx]))

        self._metadata_licenses = Gio.ListStore(item_type=LicenseType)
        for spdx in METADATA_LICENSES:
            self._metadata_licenses.append(
                LicenseType(spdx, METADATA_LICENSES[spdx]))

        factory = Gtk.SignalListItemFactory()
        factory.connect("setup", self._license_factory_setup)
        factory.connect("bind", self._license_factory_bind)
        self.licensing_project_row.set_factory(factory)
        self.licensing_project_row.set_model(self._project_licenses)
        self.licensing_project_row.connect("notify::selected-item",
                                           self._project_license_selected)

        factory = Gtk.SignalListItemFactory()
        factory.connect("setup", self._license_factory_setup)
        factory.connect("bind", self._license_factory_bind)
        self.licensing_metadata_row.set_factory(factory)
        self.licensing_metadata_row.set_model(self._metadata_licenses)
        self.licensing_metadata_row.connect("notify::selected-item",
                                            self._metadata_license_selected)

    def _license_factory_setup(self, factory, list_item):
        label = Gtk.Label()
        list_item.set_child(label)

    def _license_factory_bind(self, factory, list_item):
        label = list_item.get_child()
        license = list_item.get_item()
        label.props.label = license.license_name

    def _project_license_selected(self, row, pspec):
        self._project_license = row.get_selected_item()
        self.notify("project-license")

    def _metadata_license_selected(self, row, pspec):
        self._metadata_license = row.get_selected_item()
        self.notify("metadata-license")

    @GObject.Property(type=str)
    def project_license(self):
        return self._project_license.license_id

    @project_license.setter
    def set_project_license(self, license_id: str) -> None:
        for idx, license in enumerate(self._project_licenses):
            if license.license_id == license_id:
                self.licensing_project_row.set_selected(idx)
                break

    @GObject.Property(type=str)
    def metadata_license(self):
        return self._metadata_license.license_id

    @metadata_license.setter
    def set_metadata_license(self, license_id: str) -> None:
        for idx, license in enumerate(self._metadata_licenses):
            if license.license_id == license_id:
                self.licensing_metadata_row.set_selected(idx)
                break
