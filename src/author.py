# SPDX-FileCopyrightText: 2022  Emmanuele Bassi
# SPDX-License-Identifier: Apache-2.0

from typing import Optional

from gi.repository import Adw, GObject, Gtk


@Gtk.Template(resource_path='/io/bassi/MetaInfoBuilder/ui/author.ui')
class MetaInfoAuthorBox(Adw.Bin):
    __gtype_name__ = 'MetaInfoAuthorBox'

    author_name_row = Gtk.Template.Child()
    author_project_row = Gtk.Template.Child()
    author_contact_row = Gtk.Template.Child()

    _developer_name: Optional[str] = None
    _project_group: Optional[str] = None
    _update_contact: Optional[str] = None

    def __init__(self) -> None:
        super().__init__()
        self.author_name_row.connect("notify::text",
                                     self._update_author_name)
        self.author_project_row.connect("notify::text",
                                        self._update_author_project)
        self.author_contact_row.connect("notify::text",
                                        self._update_author_contact)

    @GObject.Property(type=str)
    def developer_name(self) -> Optional[str]:
        return self._developer_name

    @developer_name.setter
    def set_developer_name(self, name: str) -> None:
        if self._developer_name != name:
            self._developer_name = name
            self.author_name_row.set_text(name)

    @GObject.Property(type=str)
    def project_group(self) -> Optional[str]:
        return self._project_group

    @project_group.setter
    def set_project_group(self, group: str) -> None:
        if self._project_group != group:
            self._project_group = group
            self.author_project_row.set_text(group)

    @GObject.Property(type=str)
    def update_contact(self) -> Optional[str]:
        return self._update_contact

    @update_contact.setter
    def set_update_contact(self, contact: str) -> None:
        if self._update_contact != contact:
            self._update_contact = contact
            self.author_contact_row.set_text(contact)

    def _update_author_name(self, row, _):
        self._developer_name = row.get_text()
        self.notify('developer-name')

    def _update_author_project(self, row, _):
        project = row.get_text()
        if project not in ['', 'GNOME', 'KDE', 'XFCE', 'MATE', 'LXDE']:
            row.add_css_class('error')
        else:
            self._project_group = project
            row.remove_css_class('error')
            self.notify('project-group')

    def _update_author_contact(self, row, _):
        self._update_contact = row.get_text()
        self.notify('update-contact')
